.PHONY: default
default: ## Build site to /docs
	@mkdir -p docs
	@rm -rf docs/*
	@cp index.html list_filter.js style.css typeStypeM.png docs/

.PHONY: check-valid-public-site
check-valid-public-site: ## @ignored
	@if [ ! -d "../public-site" ]; then \
		echo "../public-site does not exist."; \
		exit 1; \
	fi
	@if [ ! -d "../public-site/.git" ]; then \
		echo "../public-site is not a Git repository."; \
		exit 1; \
	fi

.PHONY: publicize
publicize: check-valid-public-site ## Release to /public
	@echo Syncing to ../public-site/public
	@mkdir -p ../public-site/public/useful-references
	@rsync -rv --delete docs/ ../public-site/public/useful-references
